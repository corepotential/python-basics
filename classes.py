# Classes - Self, Initialization and Calling Functions

# Classes - Used to put together variables and functions
# Example
class Jumping: # Gives class the value of "Jumping"
    pass # This means what it says...pretty much just ignores it and moves to whats next

x = Jumping() # Gives "x" the value of Jumping
# Due to the "pass" this should result in a blank response

# Self - Used to connect variables and functions with a class
# Example
class Fitness:
    def pushUps(self):
        print ("Let's get fit!")

x = Fitness()
x.pushUps()
# If you don't have "self" in this instance you'll get an error message that should look like:
# "TypeError: pushUps() takes 0 positional arguments but 1 was given"...this is due to "self" being the argument

# Initialization - Function run when class instance is created
# Example
class Jumping:
    def __init__(self): # This is used to run the function
        self.fun = 10 # Correct way
        run = 10 # Incorrect due to not having "self." in front of it

    def pushUps(self):
        print ("Getting stronger!")

x = Jumping()
x.pushUps()
print (x.fun) # To try this add ", x.run" in print
# This will produce another error message that should look like:
# "AttributeError: 'Jumping' object has no attribute 'run'", to fix this place "self." in front of "run"
# Example 2
class Jumping:
    def __init__(self): # This is used to run the function
        self.fun = 10 # Correct way
        self.run = 15 # Correct way

    def pushUps(self):
        print ("Getting stronger!")

x = Jumping()
x.pushUps()
print (x.fun, x.run)
# Now the result should be "10" and "15" instead of the error message

# Calling Functions - Just like it says, this calls out the function given
# Example
class Skipping:
    def __init__(self):
        self.fun = 10
        self.pushUps()

    def pushUps(self):
        print ("Still getting stronger!")

x = Skipping()

# Example 2
class Trainer:
    '''
    Your personal trainer
    '''

    def __init__(self, name):
        self.name = name
        self.strength = 40

    def extraWork(self, diet):
        if diet == "junk food":
            self.strength -= 30
        elif diet == "healthy":
            self.strength += 50

Harpy = Trainer("Harpy's strength is: ")
print (Harpy.name, Harpy.strength)
# This part will result in an output of "Harpy's strength is:  40"...let's see what happens when we eat "junk food"
Harpy.extraWork("junk food")
print ("Harpy ate junk food so his strength is now: ", Harpy.strength)
# Now we get another output that should be "Harpy ate junk food so his strength is now:  10"
# Now lets see what happens when Harpy eats healthy
Harpy.extraWork("Healthy")
print ("Harpy is eating healthy again, his strength is: ", Harpy.strength + 45)
# Harpy is finally very healthy with an output of "Harpy is eating healthy again, his strength is:  55"
# The "+45" at the end will be added to whatever the previous health was