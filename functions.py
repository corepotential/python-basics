# Functions - Return, Arguments, Local vs Global Variables, Comments and Documenting
# Way to combine multiple actions into a single process, to be used in the future
# Example
def nothingHappened():
    pass
# This will give you a result of nothing

# Return - Output or return data from functions
# Example
def harpysCool():
    return 10
x = harpysCool()
print (x)
# This should give you an output of "10"

# Arguments - Input or pass in data to a function
# Example
def harpySucks(justJoking):
    justJoking += 20
    return justJoking
x = 30
print (dir())
# This will give you an output similar to:
# ['__annotations__', '__builtins__', '__cached__', '__doc__', '__file__',
# '__loader__', '__name__', '__package__','__spec__',
# 'harpySucks', 'harpysCool', 'nothingHappened', 'x']
# This is due to "dir()" acting as a directory printing out listings of "attributes" and
# "objects" in alphabetical order
# Now lets add another variable and see what happens
s = harpySucks(x)
print (s)
# Now you should get an output of "50" as you are adding "justJoking" with "x"

# Local vs Global Variables
# Local - Create and stored within a function that will be delete it's memory once function is complete
# Example - Local
def myLocal():
    harpy = 4
# Global - Accessible anywhere within a program, it's keyword is "global"
# Example - Global
harpys = 9

def myLocal():
    global harpys

# Comments and Documenting - Used mainly for debugging and keeping things easy for other coders to read/understand
# Document String - text detailing the function directly after a function is created and is made by using triple quotes
# Example - Documenting
def harpysDoc():
    '''
    Harpy is my friend but his a little weird, which i like :D!
    '''
# Comments - Tells the program to ignore whatever comes after it in the line starting with "#", used for note taking
# Example - Commenting
x = 60 # Harpy is old!
# This will only print "60" but not the rest of the line due to it being commented out
# Now without printing the document the user won't see it so lets try this:
print (x, harpysDoc.__doc__)
# Now you should get an output that says "60" and "Harpy is my friend but his a little weird, which i like :D!"
