# Lists - Data Structure, Functions, Other Functions and Extended "in" Knowledge
# Tuples - Tuples vs Lists
# Dictionaries - Accessing Values, Updating, Deleting, Properties, Functions and Methods

# Data Structure - Used for storing all data types (int,float,string,etc)
# Example
x = ["Harpy", 9, 4.5]
print (x)
# This should print out exactly whats been typed because all this is showing is the stored data and then printing it.

# Functions - "append", "insert", "pop", "del" and "remove"
# Append - Adds a value to the end of a list
# Example
x.append(10)
print (x)
# Now you should notice the "10" at the end our list we made previously

# Insert - Inserts the value to where you want it
# Example
x.insert(3,45)
print (x)
# As you can see the number "45" is now added just before the "10" as that is the 3rd position in the string.
# this is because the "3," positions the number where you want it

# Pop - Removes the item at a specific index and returns it
# Example
x.pop(2)
print (x)
# wow suddenly the "4.5" disappeared, this is because we chose to take the 2nd position in this example

# Del - Removes the item at a specific index
# Example
del x[1]
print (x)
# There you have it, position 1 is now gone!

# Remove - Removes the first matching value, not a specific index
x = ["Harpy", 9, 10, 45, 10]
x.remove(10)
print (x)
# It found the first "10" and removed it but if it was looking for a specific index it wouldn't of worked.

# Extended "in" Knowledge- Checking if something is in a list
# Example
print ("H" in "Harpy")
# This gives a "True" because it checks if there is an "H" as an item not within a string of a list.

# Tuples - Like lists but don't adjust (you can't add/remove from it)
# Example
x = tuple("Hello")
print (x)
# As you see in our output it printed each letter separately.

# Tuples vs Lists - Tuples are more memory efficient but are bad if wanting to adjust something
#                 - Lists aren't as memory efficient but are good when needing to adjust something

# Accessing Values - Values can be any type but "keys" have to be "strings", "numbers" or "tuples"
# To access dictionary elements you can use square brackets with the keys to get the value
# Example
dict = {"Name": "Harpy", "Age": "Unknown"}
print (dict["Name"], dict["Age"])
# You should get two(2) outcomes from this which are "Harpy" and "Unknown" as that is what we "accessed" from the dict.
# Lets try accessing it with a key and not a value now and see what happens
#dict = {"Name": "Harpy", "Age": "Unknown"}
#print (dict["James"], dict["James"])
# We get an error "KeyError: 'James'" which is why as explained you search using values.
# To test this just remove "#" from the lines

# Updating - You update by doing one of the following: "new entry/key-value pair", changing/deleting an existing entry.
# Example
dict = {"Name": "Harpy", "Age": "Unknown"}
dict["Age"] = 25 # Now we have updated the age of Harpy
dict["Health"] = "Good" # Here we have added a new entry

print (dict["Age"], dict["Health"])
# Now the output is different from before due to the updates we made to our dictionary

# Deleting - You can remove some or all of the dictionary using "del"
# Example - Removing only a single entry
del dict["Name"]
print (dict)
# As you have noticed "Name" doesn't appear when we print the output now
# Example - Removing all entries
dict.clear()
print (dict)
# All that remains now is {} when we get the returned output
#Example - Removing the dictionary completely
del dict
print (dict)
# Now that we don't have a dictionary and aren't trying to print something from it, we get this "<class 'dict'>"
# which is just stating what class type is being used, Now lets test what happens if we try printing a value
# Example - Trying to print a value that doesn't exist
#print (dict["Health"])
# We get an error such as "TypeError: 'type' object is not subscriptable" as expected since nothing is there to print.
# To try this remove the "#" in front of print

# Properties - There are two(2) main points to learn when using dictionary keys
# 1. You can't use more than one entry per key, if you use more than one the last assigned key will be printed
# Example
dict = {"Name": "James", "Age": 20, "Name": "Harpy"}
print (dict["Name"])
# As you can see we have two "Names" but only "Harpy" was printed
# 2. Keys must be immutable, so only use strings,numbers or tuples as dictionary keys and avoid something like ["hi"]
# because it's not allowed to be used and won't work at all
# Example
dict = {["Name"]: "Hapry"}
#print (dict[["Name"]])
# The outcome is an error message as expected that should look similar to "TypeError: unhashable type: 'list'"
# To test this remove "#" as usual