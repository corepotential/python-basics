# Files and User Inputs - File Benefits, Creating a File, Importing Modules between Files
# Python files always end in one of these - ".py", ".pyw" and ".pyc"
# .pyc - Python compile files (files only the computer can understand)
# .py - Python writeable files (general scripts that can be read by both computer and user)
# .pyw - Mostly used for to identify if a script in windows needs to be executed in "pythonw.exe" instead of "python.exe"

# File Benefits - Flexible and editable
#               - Save for reuse
#               - Don't have to re-type anything
#               - Can run whenever ready/needed

# Creating a File - Follow steps below
# Create a folder labelled "Python is fun" > create file labelled "random.py" > write inside file
print ("Super programmer!")
input ("Press keys to write scripts!")
# This should give you an output of "Super programmer" and "Press keys to write scripts!"
# for python2 users instead of "input" you'll use "raw_input"

# Fun Game using Input
highest = 20
answer = 9
chance = input ("Take a chance and guess the number from 0 to %d:" % highest)

while int(chance) != answer:
    if int(chance) < answer:
        print ("wrong...try going higher!")
    else:
        print ("wrong...try going lower")
    chance

input ("Congratulations you got it!!!")
# To play this game have it in a separate file or "#" out the other parts of code
# It should give one of two(2) outputs until you guess right: if higher "wrong...try going lower" and if lower
# "wrong...try going higher!" and once correct you should see "Congratulations you got it!!!"

# Importing Modules - Way to access functions, classes and variables from other files
# Example - Changes to the previous game
import random

highest = 20
answer = random.randrange(highest)

while int(chance) != answer:
    if int(chance) < answer:
        print ("wrong...try going higher!")
    else:
        print ("wrong...try going lower")
    chance

input ("Congratulations you got it!!!")
# Follow the same steps as in the first game

# Separating Files - Break up code into parts based on their functionality
#                  - Prevents stupidly long files
#                  - Easier to keep things organised
#                  - Has to be in the same folder/directory for it to work
# Create file called "Python is interesting.py"
def codingLife():
    print ("yay i'm a programmer!!!")
# Now create a new file labelled "main.py"
import CL

for i in range(5):
    CL.codingLife()

input ("Lines are complete...mission success!!!")

