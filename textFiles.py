# Creating Text Files

# Setting up
# Create a new folder on desktop > open IDLE > file > new window > save as "PythonIsFun"

# Functions
# Example
def exampleDoc(dest): # creates a definition for the function
    print (dest)

if __name__=='__main__': # tests to see if it works
    exampleDoc("python learning")
    input("complete")

# Importing files
# Example
import time as t #imports a module called time and values it as "t"
from os import path

def exampleDoc(dest): # this is the exact same as a comment...it's a note for other people to read
    '''
    This creates a file with location and name based on date....this helps with keeping organised!
    '''

date = t.localtime(t.time()) # gives date the value of t
#DocName = Day_Month_Year
name = "%d_%d_%d.txt" % (date[1], date[2], (date[0] % 100))

if not(path.isfile(dest + name)): # checks to see if file is opened
    f = open(dest + name, 'w')
    f.write('\n' * 10) # creates file if it doesn't exist
    f.close()

if __name__=='__main__':
    destination = 'C:\\Users\\simon\\Desktop\\new\\' # location of the document (change it to suit with you)
    exampleDoc(destination)
    input("complete!")