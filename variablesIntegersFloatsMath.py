# Conditionals - "if" "elif" "else"

# "if" Statements
tree = 1

if tree:
    print ("I like tree's")
# this will print "I like tree's" as the value of "tree" is 1 which always equals True

# "elif" Statement
if tree:
    print ("I like tree's")
elif grass:
    print ("I like grass!")
# this statement always comes after the "if" statement to set up another conditional

# "else" Statement
if tree:
    print ("I like tree's")
elif grass:
    print ("I like grass!")
else:
    print ("I don't go outside!")
# this statement picks up on everything that the previous conditionals don't meet

# Operators - Lets you compare more than one value
# <   = Less than
# <=  = Less then or equal to
# >   = Greater than
# >=  = Greater then or equal to
# ==  = Equal to
# !=  = Not equal to
# these are the same method of use in pretty much all programming languages

# Less than
if 1 < 3:
    print ("this is maths?!")
else:
    print ("maths is hard!")

# Less then or equal to
if 61 <= 50:
    print ("this is correct maths!")
else:
    print ("nope...this is wrong maths!")

# Greater than
if 5 > 4:
    print ("yay this is right!")
else:
    print ("nope...this isn't right!")

# Greater then or equal to
if 50 >= 40:
    print ("i like maths!")
else:
    print ("i don't like maths!!!")

# Equals to
if 30 == 10:
    print ("this is correct!")
else:
    print ("nope...it's wrong!")

# Not equal to
if 9 != 0:
    print ("learning is fun!")
else:
    print ("i don't like learning!")