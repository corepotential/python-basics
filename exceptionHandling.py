# Exception Handling - Try, Pass, Raise, Finally
# Exception handling stops scripts from breaking as well as being used for handling user inputs

# Try - Tries to execute the code under it
# Example
try:
    Harpy = 15 + "studying"
except:
    print ("failed...")
# The result should be "failed..." as it tried to run something without an output and went to except instead.

# Pass - Tells the program/script to skip that part of code
# Example
try:
    Harpy = 15 + "studying"
except:
    pass
# The output of this should result in nothing...just a blank space as it's passing this section of code.

# Raise - Forces an error
# Example
#raise SyntaxError("this is interesting!")
# This will force an error looking similar to this "SyntaxError: this is interesting!"
# To test just remove the "#" that's in front of "raise"

# Finally - Performed after "try" and "except", while occurs before any real error's are given
# Example
try:
    Harpy = 15 + "studying"
except SyntaxError:
    print ("can you see me?")
finally:
    print ("i could't see it?!")
# This will result in two(2) outputs, 1st being "i couldn't see it?!" and the 2nd being an error message similar to:
# TypeError: unsupported operand type(s) for +: 'int' and 'str'
# The "operand" is the same as the operator being used "+" in this case