# Loops - While, Break, for, range, continue
# Loops are ways to repeat actions with less code

# While - If condition is "True" run code over and over
# Example
x = 10

while x < 20:
    x += 3
print (x)
# The output should be "22" because it will run until it hits the 23 counts including the number "0" not "1"

# Break - Used as a stopping method
# Example
d,k = 23,25

while True:
    d += 5
    k += 10
    if d + k > 10:
        break
print (d, k)
# The output for this should be "28" and "35" as the code saw it was more than "10" and broke the loop

# For - Loops each item in lists/range
# Example
x = [10, 15, 20]

for i in x:
    print (i)
# If done correctly you should see an output of the 3 listed numbers printed under each other in the order listed

# Range - creates a list of sequential numbers and prints up to but not including the range
# Example
for i in range(20,50,6):
    print (i)
# You should see a print out of numbers from "20,26,32,38,44" which total only even numbers

# Continue - Loops over (on value is in "if in for loop") and prints only non-multiples of 3
# Example
for i in range(50):
    if not i % 3:
        continue
print (i)
# The output should return "20,26,32,38,44,49"





