# Strings - Bunch of characters, Combining strings, Combining numbers & strings
#         - Special operators and Formatting Operators

# Bunch of Characters
# Example
x = "harpy" # "harpy" is the string in this example
print (x)
# This should print an output of "harpy"

# Combining Strings - Use "+" to combine strings together
# Example
k = x  + " rules" # "x" has been given a value in the above example so you are adding that with "rules".
print (k)
# If done correctly the output should be "harpy rules"

# Combining Strings with Numbers - Use "str():" to convert something to a string
# Example
s = 20 # "s" now has the value of "20"
k = x + str(s) # this puts the number into the string and adds it to the "x" value we made earlier
               # we changed the value of "k" in this example
print (k)
# This will give an output of "harpy20"

# Special Operators - Places number values within strings
# "+" - Adds value on either side of the operator
# Example (Go back to "Combing Strings example)

# "*" - Creates new strings, making a chain of multiple copies of something
# Example
a = "Hello"
print (a * 2)
# As you should see the result is that it printed "a" aka "hello" twice, the amount depends on the number given.

# "[]" - Slice, gives the characters from the given index
# Example
print (a[4])
# The output you get should be "o" as that is the 4th letter in "hello" as "h" is equal to "0"

# "[:]" - Range slice, gives the characters from the given range
# Example
print (a[1:3])
# This prints "el" because it only counts from 1 and stops before hitting 3, so "1 = e" and "2 = 1st l"

# "in" - Membership, returns true if a character exists in the given string otherwise will return false
# Example
print ("ha" in "harpy")
# As you can see we are returned with a true as "ha" is a part of "harpy", lets try to find a false now
print ("wa" in "harpy")
# Now we have a false output

# "not in" - Very similar to "in" except it will return true if something is not in the string
# Example
print ("wa" not in "harpy")
# As you can see it's pretty much just the opposite of "in" as this will return a true, while before it gave a false!.

# "r/R" - Suppresses actual meaning of escape characters. This is exactly the same as raw strings except the raw string
# operator, the letter "r" which precedes the quotation marks. It can be either Upper or Lower case and must be placed
# immediately after the first quotation mark.
# Example
print (r"harpy")
# This simply just prints "harpy"

# "%" - Formats strings (Below is a full list of things used with "%")
# "%c" - Gives the character representation of the "ASCII" code
# Example
print ("%c" % 100)
# You should get the letter "d" as the output because in the "ASCII code" 100 is equal to "d"

# "%s" - String conversion via str() before formatting
# Example
name = "How are you?"
print ("i'm good %s" % name)
# As you can see the result should be "i'm good How are you?" because the "%s" converted "i'm good" into a string
# if you try this without the "%s" after "good" it will show an error similar to:
# "TypeError: not all arguments converted during string formatting"...give it a try to see for yourself

# "%i and %d" - Signed decimal integer
# Example
Number = 60
print ("my number is %i" % Number)
print ("my number is %d" % Number)
# Both output's are exactly the same as they aren't different at all, but it's mostly common to use "%d" only.





